#!/bin/sh
/docker/chromedriver --whitelisted-ips --port=4444 &
FOO_PID=$!
# nohup sh -c /app/chromedriver --whitelisted-ips &

flutter doctor
flutter pub get
flutter clean

# run test
flutter drive --target=test_driver/app.dart --release -d chrome

kill $FOO_PID
